name, age, occupation, movie, rating = "Jose", 33, "writer", "One more chance", 99.6

print(f"I am {name}, and I am {age} years old. I work as a {occupation}, and my rating for {movie} is {rating}%")

num1, num2, num3 = 10, 15, 12
print(num1 * num2)
print(num3 < num2)
# num2 += num3
print(num2 + num3)